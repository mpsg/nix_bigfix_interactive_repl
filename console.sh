#!/usr/bin/env bash
SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"
cmd=""
while [ "$cmd" != "quit" ]; do
  read cmd
  printf "$cmd" > $SCRIPTPATH/in_pipe
  printf '%s\r' "`cat $SCRIPTPATH/out_pipe`"
done
