#!/usr/bin/env bash
SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"
cmd=""
while [ "$cmd" != "quit" ]; do
  cmd=`cat $SCRIPTPATH/in_pipe`
  $cmd > $SCRIPTPATH/out_pipe 2>&1
done
