#!/bin/bash
SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"
script="$SCRIPTPATH/dialog.scpt"
currentUser=$(ls -l /dev/console | awk '{ print $3 }')
sudo -u ${currentUser} osascript $script
